import "./App.scss";

import Home from "./pages/Home";
import Presentation from "./componentes/Presentation";
import CoverPage from "./componentes/CoverPage";
function App() {
  return (
    <div className="App">
      <CoverPage />
      <Presentation />
      <Home />
    </div>
  );
}

export default App;
