import React, { useState } from "react";

import "./Presentation.scss";

//const Carousel = "";

function Presentation() {
  const [index, setIndex] = useState();

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };
  return (
    <div>hola</div>
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <img className="step1" src="imagen1.png" alt="primera imagen" />
        <Carousel.Caption>
          <h2>ENCUENTRA TODO TIPO DE SERVICIOS CERCA DE TI</h2>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="step2" src="imagenadopcion.png" alt="segunda imagen" />
        <Carousel.Caption>
          <h2>ADOPTA DESDE TU MOVIL</h2>
          <p>
            Puedes acceder al perfil de muchos animales que estan en adopcion y
            filtrarlos para encontrar el que mejor se adapta a ti
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="step3" src="imagenfinal.png" alt="tercera imagen" />
        <Carousel.Caption>
          <h2>
            SI ERES UNA ASOCIACION CUELGA A TUS PELUDOS PARA DARLES MAS DIFUSION
          </h2>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Presentation;

{
  /* codigo que me ha pasado Lorena para poder descargar imagenes de la carpeta public
<img src={process.env.PUBLIC_URL + '/logo.png'} alt="TDF_logo"/> */
}
